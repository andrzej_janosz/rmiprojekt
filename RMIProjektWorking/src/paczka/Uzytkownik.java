package paczka;

import java.io.Serializable;

public class Uzytkownik implements Serializable  {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public int id=0;
	public String NazwaUzytkownika=null;
	public boolean CzyAdministrator=false;
	
	public Uzytkownik(int id, String nazwa, boolean czyadmin) {
		this.id=id;
		this.NazwaUzytkownika=nazwa;
		this.CzyAdministrator=czyadmin;
		
	}

}
