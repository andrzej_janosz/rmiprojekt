package paczka;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface InterfejsSerwera extends Remote {
	
	String szukajProdukt(int wybor, String parametr) throws RemoteException;
	void kupProdukt() throws RemoteException;
	void przegladajProdukt() throws RemoteException;
	String przegladajWszystkieProdukty() throws RemoteException;
	void dodajProdukt(int id, String nazwa, String Producent, float cena, int ilosc) throws RemoteException;
	void dodajUzytkownika(int id, String nazwa, boolean czyadmin) throws RemoteException;
	Uzytkownik zwrocUzytkownika() throws RemoteException;
	void wyswietlajMenuSerwera() throws RemoteException;
}
