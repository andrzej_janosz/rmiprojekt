package paczka;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Scanner;

public class Serwer extends UnicastRemoteObject implements InterfejsSerwera {
	
	private static final long serialVersionUID = 1L;
	
	public ArrayList<Produkt> WszystkieProdukty=null;
	public ArrayList<Zamowienie> ListaZamowien=null;
	public Uzytkownik user=null;

	protected Serwer() throws RemoteException {
		super();
		// TODO Auto-generated constructor stub
		WszystkieProdukty=new ArrayList<Produkt>();
		ListaZamowien=new ArrayList<Zamowienie>();
	}
	
	@Override
	public void dodajUzytkownika(int id, String nazwa, boolean czyadmin) throws RemoteException {
		
		
		this.user=new Uzytkownik(id, nazwa, czyadmin);
		
		System.out.println("Utworzyles na serwerze uzytkownika o id: " + user.id + " nazywajacego sie: " + user.NazwaUzytkownika);
		if(czyadmin) {
			System.out.println("bedacego adminem.");
		}
		
	}

	@Override
	public String szukajProdukt(int wybor, String parametr) throws RemoteException {
		String wynikowy=null;
		if(WszystkieProdukty.size()>0) {
		
			
			
			switch(wybor) {
			case 1: 
				
				for (int i=0;i<WszystkieProdukty.size();i++) {
					if (WszystkieProdukty.get(i).NazwaProduktu.equalsIgnoreCase(parametr)) {
						Produkt SzukanyProdukt=WszystkieProdukty.get(i);
						
						return new String("Nazwa: "+ SzukanyProdukt.NazwaProduktu + " w cenie: "+ SzukanyProdukt.CenaProduktu);
					}
				} break;
				
			case 2:
				
				for (int i=0;i<WszystkieProdukty.size();i++) {
					if (WszystkieProdukty.get(i).ProducentProduktu.equalsIgnoreCase(parametr)) {
						Produkt SzukanyProdukt=WszystkieProdukty.get(i);
						return new String("Nazwa: "+ SzukanyProdukt.NazwaProduktu + " w cenie: "+ SzukanyProdukt.CenaProduktu);
					}
				} break;
				
			case 3:
				
				float cena=Float.parseFloat(parametr);
				for (int i=0;i<WszystkieProdukty.size();i++) {
					Float cenaTemp=new Float(cena);
					Float cenaTemp2=new Float(WszystkieProdukty.get(i).CenaProduktu);
					if (cenaTemp2.compareTo(cena)==0) {
						Produkt SzukanyProdukt=WszystkieProdukty.get(i);
						return new String("Nazwa: "+ SzukanyProdukt.NazwaProduktu + " w cenie: "+ SzukanyProdukt.CenaProduktu);
					}
				} break;
				
			case 4: 
				
				int SzukanaIlosc=Integer.parseInt(parametr);
				for(int i=0;i<WszystkieProdukty.size();i++) {
					if(WszystkieProdukty.get(i).IloscWMagazynie==SzukanaIlosc) {
						Produkt SzukanyProdukt=WszystkieProdukty.get(i);
						return new String("Nazwa: "+ SzukanyProdukt.NazwaProduktu + " w cenie: "+ SzukanyProdukt.CenaProduktu);
					}
				}
				
				
			default: 
				
				return new String("Zly wybor");
				
			}
			
			
		}
		
		else {
			return new String("Nie ma zadnych produktow1111");
		}
		return null;
		
		
		
		
	}

	@Override
	public void kupProdukt() throws RemoteException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void przegladajProdukt() throws RemoteException {
		
		
		
	}

	@Override
	public String przegladajWszystkieProdukty() throws RemoteException {
		if (WszystkieProdukty.size()>0) 	{
			for(int i=0;i<WszystkieProdukty.size();i++) {
				return new String("Produkt: " + WszystkieProdukty.get(i).NazwaProduktu + " id: " + WszystkieProdukty.get(i).id +
						" cena: " + WszystkieProdukty.get(i).CenaProduktu + " producent: " + WszystkieProdukty.get(i).ProducentProduktu +
						" ilosc: " + WszystkieProdukty.get(i).IloscWMagazynie);
			}
		}
		
		else {
			return new String("Nie ma zadnych produktow");
		}
		return null;
		
	}

	@Override
	public void dodajProdukt(int id, String NazwaProduktu, String Producent, float cena, int ilosc ) throws RemoteException {
		
		Produkt produkt=new Produkt(id, NazwaProduktu, Producent, cena, ilosc );
		this.WszystkieProdukty.add(produkt);
		
		System.out.println("Produkt od id: " + produkt.id +" zostal dodany na serwerze!!!");
		
		
		
		
		
	}

	@Override
	public Uzytkownik zwrocUzytkownika() throws RemoteException {
		// TODO Auto-generated method stub
		return this.user;
	}

	@Override
	public void wyswietlajMenuSerwera() throws RemoteException {
		// TODO Auto-generated method stub
		
			//if(this.user.CzyAdministrator==true) {
				System.out.println("1. Dodaj produkt.");
				System.out.println("2. Przegladaj wszystkie produkty.");
				//System.out.println("3. Wyszukaj produkt.");
				Scanner sc=new Scanner(System.in);
				String wybor=sc.nextLine();
				int wybor1=Integer.parseInt(wybor);
				sc.close();
				switch(wybor1) {
				
				//case 1: this.dodajProdukt(); break;
				case 2: this.przegladajWszystkieProdukty(); break;
				
				default: System.out.println("Nieprawidlowy wybor!!!");
						break;
						
		//		}
				
				
			}
		
		
	}

}
