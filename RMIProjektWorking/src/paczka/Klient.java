package paczka;

import java.rmi.RMISecurityManager;
import java.util.Scanner;

import javax.naming.Context;
import javax.naming.InitialContext;

public class Klient {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.setSecurityManager(new RMISecurityManager());
		String url="rmi://localhost/";
		
		try {
			Context context=new InitialContext();
			InterfejsSerwera mojObiektSerwera= (InterfejsSerwera)context.lookup(url + "MyRemoteObject");
			Scanner sc=new Scanner(System.in);
			System.out.println("Stworz uzytkownika");
			System.out.println("Podaj id uzytkownika: ");
			String id=sc.nextLine();
			int id1=Integer.parseInt(id);
			System.out.println("Podaj nazwe uzytkownika: ");
			String nazwa=sc.nextLine();
			System.out.println("Czy uzytkownik jest administratorem? Tak/Nie");
			String czyadmin=sc.nextLine();
			boolean czyadmin1=false;
			if(czyadmin.equalsIgnoreCase("tak")) {
				czyadmin1=true;
			}
			mojObiektSerwera.dodajUzytkownika(id1, nazwa, czyadmin1);
			
			Uzytkownik lokalnyuser=new Uzytkownik(id1, nazwa, czyadmin1);
			Uzytkownik remoteUser=mojObiektSerwera.zwrocUzytkownika();
			System.out.println("Utworzyles na serwerze uzytkownika o id: " + remoteUser.id + " nazywajacego sie: " + remoteUser.NazwaUzytkownika);
			
			
			
			System.out.println("Utworzyles lokalnie uzytkownika o id: " + lokalnyuser.id + " nazywajacego sie: " + lokalnyuser.NazwaUzytkownika);
			if(lokalnyuser.CzyAdministrator) {
				System.out.println("bedacego adminem.");
				System.out.println("Menu administratora: ");
				System.out.println("1. Dodaj produkt. ");
				System.out.println("2. Przegladaj liste dostepnych produktow. ");
				System.out.println("3. Szukaj produkt.");
				String wybor1=sc.nextLine();
				int wybor11=Integer.parseInt(wybor1);
				
				switch(wybor11) {
				case 1:
						System.out.println("Dodawanie produktu");
						System.out.println("Podaj id produktu: ");
						String idProduktu=sc.nextLine();
						int idNowegoProduktu=Integer.parseInt(idProduktu);
				
						System.out.println("Podaj nazwe produktu: ");
						String NazwaProduktu=sc.nextLine();
				
						System.out.println("Podaj producenta produktu: ");
						String Producent=sc.nextLine();
				
						System.out.println("Podaj cene produktu: ");
						String cenaproduktu=sc.nextLine();
						float cena=Float.parseFloat(cenaproduktu);
				
						System.out.println("Podaj ilosc sztuk produktu: ");
						String iloscwmagazynie=sc.nextLine();
						int ilosc=Integer.parseInt(iloscwmagazynie);
						
						mojObiektSerwera.dodajProdukt(idNowegoProduktu, NazwaProduktu, Producent, cena, ilosc);
						System.out.println("Produkt zosta� dodany!!!");
						//String wynik=mojObiektSerwera.przegladajWszystkieProdukty();
						//System.out.println(wynik);
						break;
						
				case 2:
						System.out.println("Wyswietlanie listy wszystkich produktow");
						String wynik1=mojObiektSerwera.przegladajWszystkieProdukty();
						System.out.println(wynik1);
						break;
						
				case 3:
						System.out.println("Wyszukiwanie produktu");
						System.out.println("Wybierz po jakim parametrze chcesz wyszukac produkt: ");
						System.out.println("1:Nazwie produktu");
						System.out.println("2:Producencie");
						System.out.println("3:Cenie");
						System.out.println("4:Ilosci sztuk w magazynie");
						String wyborszukania=sc.nextLine();
						int wyborzszukania1=Integer.parseInt(wyborszukania);
						String wynikszukania=null;
						
						switch(wyborzszukania1) {
						case 1:
							System.out.println("Podaj nazwe szukanego produktu: ");
							String NazwaSzukanegoProduktu=sc.nextLine();
							wynikszukania=mojObiektSerwera.szukajProdukt(wyborzszukania1, NazwaSzukanegoProduktu);
							System.out.println(wynikszukania);
							break;
						case 2: 
							System.out.println("Podaj producenta szukanego produktu: ");
							String szukanyproducent=sc.nextLine();
							wynikszukania=mojObiektSerwera.szukajProdukt(wyborzszukania1, szukanyproducent);
							System.out.println(wynikszukania);
							break;
						case 3:
							System.out.println("Podaj cene szukanego produktu: ");
							String cenaszukanego=sc.nextLine();
							wynikszukania=mojObiektSerwera.szukajProdukt(wyborzszukania1, cenaszukanego);
							System.out.println(wynikszukania);
							break;
						case 4:
							System.out.println("Podaj ilosc sztuk szukanego produktu: ");
							String iloszczukanego=sc.nextLine();
							wynikszukania=mojObiektSerwera.szukajProdukt(wyborzszukania1, iloszczukanego);
							System.out.println(wynikszukania);
							break;
							
							
							
							
						}
						
					
				}
			}
			
			else {
				System.out.println("nie bedacego adminem");
				System.out.println("Co chcesz kupic?");
			}
			
			
			System.in.read();
			
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
