package paczka;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface InterfejsSerwera extends Remote {
	
	void szukajProdukt() throws RemoteException;
	void kupProdukt() throws RemoteException;
	void przegladajProdukt() throws RemoteException;
	void przegladajWszystkieProdukty() throws RemoteException;
	void dodajProdukt() throws RemoteException;
	void dodajUzytkownika(int id, String nazwa, boolean czyadmin) throws RemoteException;
	Uzytkownik zwrocUzytkownika() throws RemoteException;
	void wyswietlajMenuSerwera() throws RemoteException;
	
}
