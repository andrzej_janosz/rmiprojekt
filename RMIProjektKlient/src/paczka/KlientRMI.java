package paczka;

import java.io.IOException;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.util.Scanner;

import javax.naming.Context;
import javax.naming.InitialContext;

public class KlientRMI {
	public InterfejsSerwera mojObiektSerwera=null;
	public Scanner sc=null;
	
	public KlientRMI() {
		System.setSecurityManager(new RMISecurityManager());
		String url="rmi://localhost/";
		
		
		
		try {
			Context context=new InitialContext();
			mojObiektSerwera= (InterfejsSerwera)context.lookup(url + "MyRemoteObject");
			sc=new Scanner(System.in);
			System.out.println("Stworz uzytkownika");
			System.out.println("Podaj id uzytkownika: ");
			String id=sc.nextLine();
			int id1=Integer.parseInt(id);
			System.out.println("Podaj nazwe uzytkownika: ");
			String nazwa=sc.nextLine();
			System.out.println("Czy uzytkownik jest administratorem? Tak/Nie");
			String czyadmin=sc.nextLine();
			boolean czyadmin1=false;
			if(czyadmin.equalsIgnoreCase("tak")) {
				czyadmin1=true;
			}
			
			Uzytkownik user=new Uzytkownik(id1, nazwa, czyadmin1);
			
			System.out.println("Utworzyles uzytkownika o id: " + user.id + " nazywajacego sie: " + user.NazwaUzytkownika);
			
			
			System.out.println("Czy uzytkownik ma prawa administratora: "+ user.CzyAdministrator);
			
			
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}
	
	public void menuDlaAdministratora() throws RemoteException {
		
		System.out.println("Wybierz opcj�: ");
		System.out.println("1. Dodaj nowy produkt.");
		System.out.println("2. Przegl�daj wszystkie produkty.");
		System.out.println("3. Szukaj produkt.");
		String wybor=sc.nextLine();
		int opcje=Integer.parseInt(wybor);
		
		sc.close();
		
		
		switch(opcje) {
		case 1: 
			mojObiektSerwera.dodajProdukt();
			
			break;
			
		
		
		case 2: 
			mojObiektSerwera.przegladajWszystkieProdukty();
			
			break;
			
		
		
		case 3: 
			mojObiektSerwera.szukajProdukt();
			
			break;
			
		
		
	
			
			
			
		
		}
		
		
		
		
		
	}
	

	/**
	 * @param args
	 * @throws RemoteException 
	 */
	public static void main(String[] args) throws RemoteException {
		// TODO Auto-generated method stub
		KlientRMI klient=new KlientRMI();
		
		klient.menuDlaAdministratora();
		
		
		
		
		

	}

	

}
