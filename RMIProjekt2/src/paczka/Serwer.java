package paczka;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Scanner;

public class Serwer extends UnicastRemoteObject implements InterfejsSerwera {
	
	private static final long serialVersionUID = 1L;
	
	public ArrayList<Produkt> WszystkieProdukty=null;
	public ArrayList<Zamowienie> ListaZamowien=null;
	public Uzytkownik user=null;

	protected Serwer() throws RemoteException {
		super();
		// TODO Auto-generated constructor stub
		WszystkieProdukty=new ArrayList<Produkt>();
		ListaZamowien=new ArrayList<Zamowienie>();
	}
	
	@Override
	public Uzytkownik dodajUzytkownika(int id, String nazwa, boolean czyadmin) throws RemoteException {
		
		Uzytkownik user=new Uzytkownik(id, nazwa, czyadmin);
		System.out.println("Utworzyles uzytkownika o id: " + user.id + " nazywajacego sie: " + user.NazwaUzytkownika);
		if(czyadmin) {
			System.out.println("bedacego adminem.");
		}
		
		return user;
		
	}
	

	@Override
	public void szukajProdukt() throws RemoteException {
		if(WszystkieProdukty.size()>0) {
		
			System.out.println("Szukaj po: ");
			System.out.println("1:Nazwie produktu");
			System.out.println("2:Producencie");
			System.out.println("3:Cenie");
			System.out.println("4:Ilosci sztuk w magazynie");
			Scanner sc=new Scanner(System.in);
			String wyborS=sc.nextLine();
			int wybor=Integer.parseInt(wyborS);
			
			switch(wybor) {
			case 1: 
				System.out.println("Podaj nazwe szukanego produktu:");
				String NazwaSzukanegoProduktu=sc.nextLine();
				for (int i=0;i<WszystkieProdukty.size();i++) {
					if (WszystkieProdukty.get(i).NazwaProduktu.equalsIgnoreCase(NazwaSzukanegoProduktu)) {
						Produkt SzukanyProdukt=WszystkieProdukty.get(i);
						System.out.println("Znaleziono produkt");
						SzukanyProdukt.wypiszDaneProduktu();
					}
				} break;
				
			case 2:
				System.out.println("Podaj producenta szukanego produktu: ");
				String NazwaSzukanegoProducenta=sc.nextLine();
				for (int i=0;i<WszystkieProdukty.size();i++) {
					if (WszystkieProdukty.get(i).ProducentProduktu.equalsIgnoreCase(NazwaSzukanegoProducenta)) {
						Produkt SzukanyProdukt=WszystkieProdukty.get(i);
						System.out.println("Znaleziono produkt");
						SzukanyProdukt.wypiszDaneProduktu();
					}
				} break;
				
			case 3:
				System.out.println("Podaj cene szukanego produktu:");
				float CenaSzukanegoProduktu=sc.nextFloat();
				for (int i=0;i<WszystkieProdukty.size();i++) {
					Float cenaTemp=new Float(CenaSzukanegoProduktu);
					Float cenaTemp2=new Float(WszystkieProdukty.get(i).CenaProduktu);
					if (cenaTemp2.compareTo(CenaSzukanegoProduktu)==0) {
						Produkt SzukanyProdukt=WszystkieProdukty.get(i);
						System.out.println("Znaleziono produkt");
						SzukanyProdukt.wypiszDaneProduktu();
					}
				} break;
				
			case 4: 
				System.out.println("Podaj ilosc sztuk danego produktu: ");
				String szukanaIlosc=sc.nextLine();
				int SzukanaIlosc=Integer.parseInt(szukanaIlosc);
				for(int i=0;i<WszystkieProdukty.size();i++) {
					if(WszystkieProdukty.get(i).IloscWMagazynie==SzukanaIlosc) {
						Produkt SzukanyProdukt=WszystkieProdukty.get(i);
						System.out.println("Znaleziono produkt");
						SzukanyProdukt.wypiszDaneProduktu();
					}
				}
				
				
			default: 
				System.out.println("Dokonales niewlasciwego wyboru");
				
			}
			sc.close();
			
		}
		
		else {
			System.out.println("Nie ma zadnych produktow1111");
		}
		
		
		
		
	}

	@Override
	public void kupProdukt() throws RemoteException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void przegladajProdukt() throws RemoteException {
		
		
		
	}

	@Override
	public void przegladajWszystkieProdukty() throws RemoteException {
		if (WszystkieProdukty.size()>0) 	{
			for(int i=0;i<WszystkieProdukty.size();i++) {
				System.out.println("Produkt: " + WszystkieProdukty.get(i).NazwaProduktu + " id: " + WszystkieProdukty.get(i).id +
						" cena: " + WszystkieProdukty.get(i).CenaProduktu + " producent: " + WszystkieProdukty.get(i).ProducentProduktu +
						" ilosc: " + WszystkieProdukty.get(i).IloscWMagazynie);
			}
		}
		
		else {
			System.out.println("Nie ma zadnych produktow");
		}
		
	}

	@Override
	public void dodajProdukt() throws RemoteException {
		Scanner sc=new Scanner(System.in);
		System.out.println("Dodawanie produktu");
		System.out.println("Podaj id produktu: ");
		String idProduktu=sc.nextLine();
		int id=Integer.parseInt(idProduktu);
		
		System.out.println("Podaj nazwe produktu: ");
		String NazwaProduktu=sc.nextLine();
		
		System.out.println("Podaj producenta produktu: ");
		String Producent=sc.nextLine();
		
		System.out.println("Podaj cene produktu: ");
		String cenaproduktu=sc.nextLine();
		float cena=Float.parseFloat(cenaproduktu);
		
		System.out.println("Podaj ilosc sztuk produktu: ");
		String iloscwmagazynie=sc.nextLine();
		int ilosc=Integer.parseInt(iloscwmagazynie);
		
		this.WszystkieProdukty.add(new Produkt(id, NazwaProduktu, Producent, cena, ilosc ));
		
		System.out.println("Produkt zostal dodany!!!");
		sc.close();
		
		
		
		
	}

	@Override
	public Uzytkownik zwrocUzytkownika() throws RemoteException {
		// TODO Auto-generated method stub
		return this.user;
	}

	@Override
	public void wyswietlajMenuSerwera() throws RemoteException {
		// TODO Auto-generated method stub
		
			//if(this.user.CzyAdministrator==true) {
				System.out.println("1. Dodaj produkt.");
				System.out.println("2. Przegladaj wszystkie produkty.");
				//System.out.println("3. Wyszukaj produkt.");
				Scanner sc=new Scanner(System.in);
				String wybor=sc.nextLine();
				int wybor1=Integer.parseInt(wybor);
				sc.close();
				switch(wybor1) {
				
				case 1: this.dodajProdukt(); break;
				case 2: this.przegladajWszystkieProdukty(); break;
				
				default: System.out.println("Nieprawidlowy wybor!!!");
						break;
						
		//		}
				
				
			}
		
		
	}

	@Override
	public int zwrocIdUzytkownika() throws RemoteException {
		// TODO Auto-generated method stub
		return this.user.id;
	}

	@Override
	public String zwrocNazweUzytkownika() throws RemoteException {
		// TODO Auto-generated method stub
		return this.user.NazwaUzytkownika;
	}

	@Override
	public boolean zwrocCzyUZytkownikToAdministrator() throws RemoteException {
		// TODO Auto-generated method stub
		return this.user.CzyAdministrator;
	}

}
