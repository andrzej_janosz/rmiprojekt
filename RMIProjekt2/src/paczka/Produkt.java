package paczka;

public class Produkt {
	
	public int id=0;
	public String NazwaProduktu=null;
	public String ProducentProduktu=null;
	public float CenaProduktu=0;
	public int IloscWMagazynie=0;
	
	
	public Produkt(int id, String Nazwa, String Producent, float Cena, int ilosc) {
		this.id=id;
		this.NazwaProduktu=Nazwa;
		this.ProducentProduktu=Producent;
		this.CenaProduktu=Cena;
		this.IloscWMagazynie=ilosc;
		
	}
	
	public void wypiszDaneProduktu() {
		System.out.println("id: " + this.id);
		System.out.println("nazwa: " + this.NazwaProduktu);
		System.out.println("producent: " + this.ProducentProduktu);
		System.out.println("cena: " + this.CenaProduktu);
		System.out.println("ilosc w magazynie: " + this.IloscWMagazynie);
		
	}

}
