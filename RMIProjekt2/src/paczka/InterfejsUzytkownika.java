package paczka;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface InterfejsUzytkownika extends Remote {
	int zwrocID() throws RemoteException;
	String zwrocNazwe() throws RemoteException;
	boolean zwrocCzyAdmin() throws RemoteException;

}
