package paczka;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface InterfejsSerwera extends Remote {
	
	void szukajProdukt() throws RemoteException;
	void kupProdukt() throws RemoteException;
	void przegladajProdukt() throws RemoteException;
	void przegladajWszystkieProdukty() throws RemoteException;
	void dodajProdukt() throws RemoteException;
	Uzytkownik dodajUzytkownika(int id, String nazwa, boolean czyadmin) throws RemoteException;
	Uzytkownik zwrocUzytkownika() throws RemoteException;
	void wyswietlajMenuSerwera() throws RemoteException;
	int zwrocIdUzytkownika() throws RemoteException;
	String zwrocNazweUzytkownika() throws RemoteException;
	boolean zwrocCzyUZytkownikToAdministrator() throws RemoteException;
}
