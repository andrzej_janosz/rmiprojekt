package paczka;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class Uzytkownik extends UnicastRemoteObject implements InterfejsUzytkownika {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public int id=0;
	public String NazwaUzytkownika=null;
	public boolean CzyAdministrator=false;
	
	public Uzytkownik(int id, String nazwa, boolean czyadmin) throws RemoteException {
		this.id=id;
		this.NazwaUzytkownika=nazwa;
		this.CzyAdministrator=czyadmin;
		
	}

	@Override
	public int zwrocID() throws RemoteException {
		// TODO Auto-generated method stub
		return this.id;
	}

	@Override
	public String zwrocNazwe() throws RemoteException {
		// TODO Auto-generated method stub
		return this.NazwaUzytkownika;
	}

	@Override
	public boolean zwrocCzyAdmin() throws RemoteException {
		// TODO Auto-generated method stub
		return this.CzyAdministrator;
	}

}
